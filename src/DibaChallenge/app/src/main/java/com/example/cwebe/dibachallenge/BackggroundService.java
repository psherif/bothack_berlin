package com.example.cwebe.dibachallenge;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.cwebe.dibachallenge.chat.MainActivity;
import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.IEventHandler;
import com.example.cwebe.dibachallenge.eventHandling.event.IBaseEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.LocationPermsissionGrantedEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.RequestLocationPermissionEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.ScheduledTimeEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserChatLocationEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserChatTextevent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserLocationOutputEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserLocationUpdateEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserOutputEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextOutputEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;


/**
 * Created by psherif on 20/11/16.
 */

public class BackggroundService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, IEventHandler {

    GoogleApiClient mGoogleApiClient;

    public static final long NOTIFY_INTERVAL =  10000;//15 * 60 * 1000; // 10 seconds

    private static String TAG = BackggroundService.class.toString();

    private Timer mTimer = null;

    private Looper mServiceLooper;

    @Override
    public void onCreate() {

        Log.i(TAG,"***** onCreate ****");
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initService();
        //return super.onStartCommand(intent, flags, startId);

       return START_STICKY;
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);

    }

    private void initService(){
        Log.i(TAG,"***** initService ****");

        // cancel if already existed
        if(mTimer != null) {
            mTimer.cancel();
            mTimer = new Timer();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                triggerTimerEvent();
            }
        }, 0, NOTIFY_INTERVAL);

        EventQueue.registerEventHandler(this, LocationPermsissionGrantedEvent.class);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        EventQueue.registerEventHandler(this, UserTextOutputEvent.class);

        EventQueue.registerEventHandler(this, UserLocationOutputEvent.class);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        Log.i(this.getClass().toString(), "onConnected");

        LocationRequest req = new LocationRequest();
        req.setFastestInterval(15);
        req.setInterval(60);
        req.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, req, this);

            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            EventQueue.sendEvent(new UserLocationUpdateEvent(mLastLocation));

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(this.getClass().toString(), "onConnectionSuspended "+i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(this.getClass().toString(), "onConnectionFailed "+connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.i(TAG, "*** trigger UserLocationUpdateEvent ***");
        EventQueue.sendEvent(new UserLocationUpdateEvent(location));

        UserLocationOutputEvent event = new UserLocationOutputEvent("You are now here",location);
        EventQueue.sendEvent(event);

    }

    private void triggerTimerEvent(){
        Log.i(TAG, "*** trigger ScheduledTimeEvent ***");

        Log.i(TAG, "*** app active: "+DibApp.getInstance()+"***");
        EventQueue.sendEvent(new ScheduledTimeEvent());

    }

    public void createNotification(UserOutputEvent event){

        Log.i(this.getClass().toString(), "createNotification");
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.cast_ic_notification_small_icon)
                        .setContentTitle("Info")
                        .setContentText(event.getText());

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.addFlags(/*Intent.FLAG_ACTIVITY_CLEAR_TOP
                | */Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("text", event.getText());
        if(event instanceof  UserLocationOutputEvent){
            resultIntent.putExtra("location", ((UserLocationOutputEvent)event).getLocation());
        }
// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
// Sets an ID for the notification
        int mNotificationId = getNotificationId();
// Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }

    @Override
    public void processEvent(IBaseEvent event) {
        if(event instanceof LocationPermsissionGrantedEvent){
            mGoogleApiClient.connect();
        }
        if (event instanceof UserLocationOutputEvent) {
            UserLocationOutputEvent ev = (UserLocationOutputEvent) event;
            if(DibApp.getInstance().getCurrentActivity()!=null){
                EventQueue.sendEvent(new UserChatLocationEvent(ev.getText(), ev.getLocation()));
            }else{
                createNotification(ev);
            }
            return;
        }
        if (event instanceof UserTextOutputEvent) {
            UserTextOutputEvent ev = (UserTextOutputEvent) event;
            if(DibApp.getInstance().getCurrentActivity()!=null){
                EventQueue.sendEvent(new UserChatTextevent(ev.getText()));
            }else{
                createNotification(ev);
            }
            return;
        }
        if (event instanceof RequestLocationPermissionEvent) {
            UserTextOutputEvent ev = (UserTextOutputEvent) event;
            if(DibApp.getInstance().getCurrentActivity()!=null){
                EventQueue.sendEvent(new UserChatTextevent(ev.getText()));
            }else{
                createNotification(ev);
            }
            return;
        }
    }

    static int notificationID = 0;
    private int getNotificationId(){
        return notificationID++;
    }
}