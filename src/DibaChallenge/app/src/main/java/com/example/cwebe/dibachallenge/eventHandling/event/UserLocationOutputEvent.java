package com.example.cwebe.dibachallenge.eventHandling.event;

import android.location.Location;

/**
 * Created by psherif on 19/11/16.
 */

public class UserLocationOutputEvent extends UserOutputEvent {


    Location location;

    public UserLocationOutputEvent(String text, Location location) {
        super(text);
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public UserLocationOutputEvent(String text) {
        super(text);
    }
}
