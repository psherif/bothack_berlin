package com.example.cwebe.dibachallenge.eventHandling.event;

import android.location.Location;

/**
 * Created by psherif on 19/11/16.
 */

public class ModifyCashStatusEvent implements IBaseEvent {

    public ModifyCashStatusEvent(String account, String purpose, double transactionAmount, Location location) {
        this.transactionAmount = transactionAmount;
        this.purpose = purpose;
        this.account = account;
        this.location = location;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public String getPurpose() {
        return purpose;
    }

    public Location getLocation() {
        return location;
    }

    public String getAccount() {
        return account;
    }

    double transactionAmount;
    String purpose;
    String account;
    Location location;
}
