package com.example.cwebe.dibachallenge;

import android.location.Address;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by christoph on 19/11/16.
 */

public class DataHandler {


    static List<Address> parseATMJson() {

        List<Address> locs = new ArrayList<Address>();

        try{
            InputStream is = DibApp.getInstance().getAssets().open("testdata/atm.json");
            JsonReader jsreader = new JsonReader(new InputStreamReader(is, "UTF-8"));
            jsreader.beginArray();

            while(jsreader.hasNext())
            {
                jsreader.beginObject();

                Address address = new Address(Locale.getDefault());
                //"bigger_location":"A 10 Center II","zipcode":"15745","city":"Wildau","street":"Chausseestraße 1","location":"Südeingang im Windfang"}

                while (jsreader.hasNext()) {
                    String name = jsreader.nextName();
                    if (name.equals("bigger_location")) {
                       address.setFeatureName(jsreader.nextString());
                    } else if (name.equals("zipcode")) {
                        address.setPostalCode(jsreader.nextString());
                    } else if (name.equals("city")) {
                        address.setLocality(jsreader.nextString());
                    } else if (name.equals("street")) {
                        address.setAddressLine(0,jsreader.nextString());
                    } else {
                        jsreader.skipValue();
                    }
                }
                jsreader.endObject();
                if(address != null && address.getLocality() != null && address.getLocality().equalsIgnoreCase("Berlin")) {
                    locs.add(address);
                }
            }

            jsreader.endArray();
            jsreader.close();

        }

        catch (IOException e)
        {
            System.out.println("noo");
        }
        return locs;
    }
}
