package com.example.cwebe.dibachallenge;

import com.example.cwebe.dibachallenge.eventHandling.IEventHandler;
import com.example.cwebe.dibachallenge.eventHandling.event.ATMInProximityEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.IBaseEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateBotContext;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextInputEvent;

import java.util.Map;

/**
 * Created by cwebe on 11/19/2016.
 */

public class ChatBotManager implements IEventHandler{

    Map<String, Object> context = null;
    @Override
    public void processEvent(IBaseEvent event) {

        // handle user text input
        if (event instanceof  UserTextInputEvent) {
            UserTextInputEvent e = (UserTextInputEvent) event;
            ChatBotAIIntegration msg = new ChatBotAIIntegration(e.getText(), context);
            msg.execute();
        }
        // update conversation context from watson conversation service
        if (event instanceof UpdateBotContext)
        {
            UpdateBotContext b = (UpdateBotContext) event;
            this.context = b.GetContext();
        }

        if (event instanceof ATMInProximityEvent)
        {
            ChatBotAIIntegration msg = new ChatBotAIIntegration("can you show me atm", context);
            msg.execute();
        }
    }
    public ChatBotManager()
    {
    }
}
