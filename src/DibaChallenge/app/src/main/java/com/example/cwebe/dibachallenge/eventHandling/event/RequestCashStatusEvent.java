package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

/**
 * Called Background services:
 * Ask user how much money(CASH) was spent today /
 * in the e.g. restaurant he stayed for 2h
 */
public class RequestCashStatusEvent implements IBaseEvent {
    public RequestCashStatusEvent(String account, String purpose, Boolean countdown){
        this.account = account;
        this.purpose = purpose;
        this.countdown = countdown;
    }

    public String getAccount() {
        return account;
    }

    public String getPurpose() {
        return purpose;
    }

    private String account;

    public Boolean getCountdown() {
        return countdown;
    }

    private String purpose;
    private Boolean countdown;
}
