package com.example.cwebe.dibachallenge.eventHandling.event;

import android.location.Location;

/**
 * Created by psherif on 20/11/16.
 */

public class UserLocationUpdateEvent implements IBaseEvent {
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public UserLocationUpdateEvent(Location location) {
        this.location = location;
    }

    Location location;
}
