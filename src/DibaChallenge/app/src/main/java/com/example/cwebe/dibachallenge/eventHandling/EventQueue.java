package com.example.cwebe.dibachallenge.eventHandling;


import com.example.cwebe.dibachallenge.eventHandling.event.IBaseEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cwebe on 11/19/2016.
 */

public class EventQueue {

    private static EventQueue getInstance() {
        if(instance == null){
            instance =  new EventQueue();
        }
        return instance;
    }

    static EventQueue instance;

    public static void registerEventHandler(IEventHandler handler, Class<? extends IBaseEvent> eventType){
        getInstance()._registerListener(handler, eventType);
    };

    public static void sendEvent(IBaseEvent event){
        getInstance()._sendEvent(event);
    }


    Map<Class<? extends IBaseEvent>,IEventHandler> eventHandlerMap = new HashMap<>();

    private void _registerListener(IEventHandler handler, Class<? extends IBaseEvent> eventType){
        eventHandlerMap.put(eventType, handler);
    }


    public void _sendEvent(IBaseEvent event){
        IEventHandler handler = eventHandlerMap.get(event.getClass());
        if(handler != null){
            handler.processEvent(event);
        }
    }


}
