package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

/**
 *  UI text input
 */
public class UserTextInputEvent extends UserOutputEvent {

    public UserTextInputEvent(String text) {
        super(text);
    }
}
