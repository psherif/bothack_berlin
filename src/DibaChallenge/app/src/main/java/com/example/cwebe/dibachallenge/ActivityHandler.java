package com.example.cwebe.dibachallenge;


/**
 * Created by cwebe on 11/19/2016.
 */

public class ActivityHandler {

    /**
     *  Called Background services:
     *  geo location triggered
     */
    void TriggerAtmProximity(){};

    /**
     *  Called Background services:
     *  account history triggered
     */

    void TriggerCashIsEmpty(){};

    /**
     * Called Background services:
     * Ask user how much money(CASH) was spent today /
     * in the e.g. restaurant he stayed for 2h
     */
    void TriggerCashStatus(){};

    /**
     *  UI text input
     */
    void UserTextInput(String userInput){};

    void UserTextResponse(String watsonReply)
    {
        System.out.println("Watson reply: " + watsonReply);
    };


    /////

    void UpdateCashData(int total){};
    void UpdateCashDeltaData(int delta){};

}
