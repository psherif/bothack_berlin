package com.example.cwebe.dibachallenge.chat.model;

/**
 * Created by madhur on 17/01/15.
 */
public enum UserType {
    BOT, USER
};
