package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 20/11/16.
 */

public class UserChatOutputEvent extends UserOutputEvent {
    public UserChatOutputEvent(String text) {
        super(text);
    }
}
