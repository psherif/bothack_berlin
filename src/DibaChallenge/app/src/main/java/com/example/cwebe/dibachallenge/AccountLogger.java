package com.example.cwebe.dibachallenge;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextOutputEvent;

import java.util.Date;

/**
 * Created by christoph on 19/11/16.
 */
public class AccountLogger extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AccountBase.db";
    private static final int DATABASE_VERSION = 2;

    public static final String ACCOUNT_TABLE_NAME = "accountlog";
    public static final String ACCOUNT_COLUMN_ID = "_id";
    public static final String ACCOUNT_COLUMN_ACCOUNT = "account";
    public static final String ACCOUNT_COLUMN_AMOUNT = "amount";
    public static final String ACCOUNT_COLUMN_PURPOSE = "purpose";
    public static final String ACCOUNT_COLUMN_DATE = "date";
    public static final String ACCOUNT_COLUMN_LOCATION_LAT = "latitude";
    public static final String ACCOUNT_COLUMN_LOCATION_LON = "longitude";

    public AccountLogger(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        truncate();
    }

    public void truncate(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + ACCOUNT_TABLE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + ACCOUNT_TABLE_NAME +
                        "(" +     ACCOUNT_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                                  ACCOUNT_COLUMN_ACCOUNT + " TEXT, " +
                                  ACCOUNT_COLUMN_AMOUNT + " REAL, " +
                                  ACCOUNT_COLUMN_PURPOSE + " TEXT, " +
                                  ACCOUNT_COLUMN_DATE + " INTEGER, " +
                                  ACCOUNT_COLUMN_LOCATION_LAT + " REAL, " +
                                  ACCOUNT_COLUMN_LOCATION_LON + " REAL)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ACCOUNT_TABLE_NAME);
        onCreate(db);
    }

    public boolean logEvent(double amount, String chargedAccount, String purposeOfTransfer, Location locationOfTransfer){
        Date date = new Date();
        return logEvent(amount,chargedAccount,purposeOfTransfer, locationOfTransfer, date);
    }

    public boolean logEvent(double amount, String chargedAccount, String purposeOfTransfer, Location locationOfTransfer, Date date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(ACCOUNT_COLUMN_ACCOUNT, chargedAccount);
        contentValues.put(ACCOUNT_COLUMN_AMOUNT, amount);
        contentValues.put(ACCOUNT_COLUMN_PURPOSE, purposeOfTransfer);
        contentValues.put(ACCOUNT_COLUMN_DATE, date.getTime());
        if (locationOfTransfer != null) {
            contentValues.put(ACCOUNT_COLUMN_LOCATION_LAT, locationOfTransfer.getLatitude());
            contentValues.put(ACCOUNT_COLUMN_LOCATION_LON, locationOfTransfer.getLongitude());
        }
        db.insert(ACCOUNT_TABLE_NAME, null, contentValues);
        UserTextOutputEvent event = new UserTextOutputEvent("Account: "+chargedAccount+" Purpose: "+purposeOfTransfer+" -> "+this.getBalance("Wallet")+"€");
        EventQueue.sendEvent(event);
        System.out.println("Account: "+chargedAccount+" Purpose: "+purposeOfTransfer+" -> "+amount+"€");
        return true;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, ACCOUNT_TABLE_NAME);
        return numRows;
    }

    //public boolean updatePerson(Integer id, String name, String gender, int age) {
    //    SQLiteDatabase db = this.getWritableDatabase();
    //   ContentValues contentValues = new ContentValues();
    //    contentValues.put(PERSON_COLUMN_NAME, name);
    //    contentValues.put(PERSON_COLUMN_GENDER, gender);
    //    contentValues.put(PERSON_COLUMN_AGE, age);
    //    db.update(PERSON_TABLE_NAME, contentValues, PERSON_COLUMN_ID + " = ? ", new String[] { Integer.toString(id) } );
    //    return true;
    //}

    //public Integer deletePerson(Integer id) {
    //    SQLiteDatabase db = this.getWritableDatabase();
    //    return db.delete(PERSON_TABLE_NAME,
    //            PERSON_COLUMN_ID + " = ? ",
    //            new String[] { Integer.toString(id) });
    //}

    public Cursor getEntry(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " WHERE " +
                ACCOUNT_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }

    public long GetSecondsTillEmpty(String accountName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;
        if(accountName.isEmpty()){
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " ORDER BY "+ACCOUNT_COLUMN_DATE+" ASC", new String[]{accountName});
        } else{
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " WHERE " +
                    ACCOUNT_COLUMN_ACCOUNT + "=? ORDER BY "+ACCOUNT_COLUMN_DATE+" ASC", new String[]{accountName});
        }
        double balance = 0.0;
        double sumDelta = 0.0;
        res.moveToFirst();
        long minDate = res.getLong(res.getColumnIndex(ACCOUNT_COLUMN_DATE));
        long maxDate = res.getLong(res.getColumnIndex(ACCOUNT_COLUMN_DATE));
        while (!res.isAfterLast())
        {
            double delta = res.getDouble(res.getColumnIndex(ACCOUNT_COLUMN_AMOUNT));
            long date = res.getLong(res.getColumnIndex(ACCOUNT_COLUMN_DATE));
            if(delta < 0.0) {
                if(date < minDate) minDate = date;
                if(date > maxDate) maxDate = date;
                sumDelta -= delta;
            }
            balance += delta;
            res.moveToNext();
        }
        return Math.round((balance / (sumDelta / (maxDate - minDate)))/1000.0);
    }

    public double moneySpentForItem(String purposeText)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  null;
        if(purposeText.isEmpty())
        {
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME, new String[]{purposeText});
        }
        else{
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " WHERE " +
                    ACCOUNT_COLUMN_PURPOSE + "=?", new String[]{purposeText});
        }

        double spent = 0.0;
        res.moveToFirst();
        while (!res.isAfterLast())
        {
            double sDelt = res.getDouble(res.getColumnIndex(ACCOUNT_COLUMN_AMOUNT));
            if(sDelt<0.0)
                spent -= sDelt;
            res.moveToNext();
        }
        return spent;
    }

    public double getBalance(String accountName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = null;
        if(accountName.isEmpty()){
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " ORDER BY "+ACCOUNT_COLUMN_DATE+" ASC", new String[]{accountName});
        } else{
            res =  db.rawQuery("SELECT * FROM " + ACCOUNT_TABLE_NAME + " WHERE " +
                    ACCOUNT_COLUMN_ACCOUNT + "=? ORDER BY "+ACCOUNT_COLUMN_DATE+" ASC", new String[]{accountName});
        }
        double balance = 0.0;
        res.moveToFirst();
        while (!res.isAfterLast())
        {
            balance += res.getDouble(res.getColumnIndex(ACCOUNT_COLUMN_AMOUNT));
            res.moveToNext();
        }
        return balance;
    }

    public Cursor getAllPersons() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + ACCOUNT_TABLE_NAME, null );
        return res;
    }

}
