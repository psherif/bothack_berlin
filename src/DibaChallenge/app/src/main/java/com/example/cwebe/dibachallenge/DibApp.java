package com.example.cwebe.dibachallenge;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;

import com.example.cwebe.dibachallenge.chat.NativeLoader;
import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.event.ATMInProximityEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToDiBaATM;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToNearestATM;
import com.example.cwebe.dibachallenge.eventHandling.event.ModifyCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.RequestCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateBotContext;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextInputEvent;

/**
 * Created by psherif on 19/11/16.
 */

public class DibApp extends Application {

        private static DibApp Instance;
        public static volatile Handler applicationHandler = null;
        private ChatBotManager chatBotManager = null;
        private AccountManager accountManager = null;
        private AccountLogger accountLogger = null;

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    private Activity currentActivity;

        @Override
        public void onCreate() {
            super.onCreate();

            Instance=this;

            applicationHandler = new Handler(getInstance().getMainLooper());

            NativeLoader.initNativeLibs(DibApp.getInstance());

            chatBotManager = new ChatBotManager();
            EventQueue.registerEventHandler(chatBotManager, UserTextInputEvent.class);
            EventQueue.registerEventHandler(chatBotManager, UpdateBotContext.class);
            EventQueue.registerEventHandler(chatBotManager, ATMInProximityEvent.class);

            accountLogger = new AccountLogger(this);
            accountManager = new AccountManager(accountLogger);
            EventQueue.registerEventHandler(accountManager, ModifyCashStatusEvent.class);
            EventQueue.registerEventHandler(accountManager, UpdateCashStatusEvent.class);
            EventQueue.registerEventHandler(accountManager, RequestCashStatusEvent.class);
            EventQueue.registerEventHandler(accountManager, GuideMeToNearestATM.class);
            EventQueue.registerEventHandler(accountManager, GuideMeToDiBaATM.class);
        }

        public static DibApp getInstance()
        {
            return Instance;
        }
}
