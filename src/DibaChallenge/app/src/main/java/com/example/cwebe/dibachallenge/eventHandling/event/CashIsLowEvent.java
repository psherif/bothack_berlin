package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

/**
 *  Called Background services:
 *  account history triggered
 */
public class CashIsLowEvent implements IBaseEvent {

    public CashIsLowEvent(String accountName, double currentBalance){
        this.accountName = accountName;
        this.currentBalance = currentBalance;
        this.secondsTillEmpty = 0;
    }

    public CashIsLowEvent(String accountName, double currentBalance, long secondsTillEmpty){
        this.accountName = accountName;
        this.currentBalance = currentBalance;
        this.secondsTillEmpty = secondsTillEmpty;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public String getAccountName() {
        return accountName;
    }

    public long getSecondsTillEmpty() {
        return secondsTillEmpty;
    }

    double currentBalance;

    String accountName;

    long secondsTillEmpty;
}
