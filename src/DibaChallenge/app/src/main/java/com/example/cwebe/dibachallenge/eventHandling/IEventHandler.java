package com.example.cwebe.dibachallenge.eventHandling;

import com.example.cwebe.dibachallenge.eventHandling.event.IBaseEvent;

/**
 * Created by psherif on 19/11/16.
 */

public interface IEventHandler<T extends IBaseEvent> {
    public void processEvent(T event);
}
