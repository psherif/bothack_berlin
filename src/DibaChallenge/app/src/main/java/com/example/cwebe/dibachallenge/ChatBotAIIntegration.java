package com.example.cwebe.dibachallenge;

import android.os.AsyncTask;

import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToDiBaATM;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToNearestATM;
import com.example.cwebe.dibachallenge.eventHandling.event.ModifyCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.RequestCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateBotContext;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextOutputEvent;
import com.ibm.watson.developer_cloud.conversation.v1.ConversationService;
import com.ibm.watson.developer_cloud.conversation.v1.model.Entity;
import com.ibm.watson.developer_cloud.conversation.v1.model.Intent;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageRequest;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by cwebe on 11/19/2016.
 */

class ChatBotAIIntegration extends AsyncTask<Void, Void, Void>
{
    String question;
    Map<String, Object> context = null;

    public ChatBotAIIntegration(String question, Map<String, Object> context)
    {
        this.question = question;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        try {
            ConversationService m_service;
            m_service = new ConversationService("2016-09-20");
            m_service.setUsernameAndPassword("f1e32c56-d4ee-48ef-801c-757561ae1adc", "w5sskiQOH0Ij");
            String workspaceId = "c55429f7-85f3-43b6-b2e5-0c55e2c0fa52";

            MessageRequest newMessage = new MessageRequest.Builder()
                    .inputText(this.question)
                    .context(this.context)
                    .build();


            MessageResponse response = m_service.message(workspaceId, newMessage).execute();

            EventQueue.sendEvent(new UpdateBotContext(response.getContext()));

            if (response.getText().size() == 0)
                System.out.println("hmm...");
            else {

                if (!ParseIntents(response)) {
                    List<String> texts = response.getText();
                    EventQueue.sendEvent(new UserTextOutputEvent(texts.get(texts.size() - 1)));
                }
            }
        } catch ( Exception e)
        {
            EventQueue.sendEvent( new UserTextOutputEvent("[Luke:] I can’t believe it. \n [Yoda:] That is why you fail."));
        }
        return null;
    }

    boolean ParseIntents(MessageResponse response)
    {
        // Check for if cash money was spent
        boolean somethingTriggered = false;
        double moneySpent = 0;
        String categorySpent = "";

        List<String> texts = response.getText();
        if (texts.get(texts.size() - 1).equalsIgnoreCase("I'm sorry, but I didn't get that ...") )
            return false;

        for (Intent i: response.getIntents()
             ) {
            moneySpent = 0;
            if (i.getIntent().equalsIgnoreCase("statement_spent_money"))
            {
                for (Entity j : response.getEntities())
                {
                    if (j.getEntity().equalsIgnoreCase("clothes"))
                        categorySpent = "clothes";

                    if (j.getEntity().equalsIgnoreCase("food"))
                        categorySpent = "food";

                    if (j.getEntity().equalsIgnoreCase("sys-currency"))
                        moneySpent = Float.valueOf(j.getValue());


                }
                // if no category available, use geo location to suggest one
                if (!categorySpent.equals("") && moneySpent != 0)
                {
                    EventQueue.sendEvent(new ModifyCashStatusEvent(AccountManager.WALLET,categorySpent,-moneySpent, null));
                    somethingTriggered=true;
                } else if ( moneySpent != 0)
                {
                        EventQueue.sendEvent(new ModifyCashStatusEvent(AccountManager.WALLET,"unknown",-moneySpent, null));
                        somethingTriggered=true;
                }
            }


            // UpdateWallet
            if (i.getIntent().equalsIgnoreCase("UpdateWallet"))
            {
                moneySpent = 0;
                for (Entity j : response.getEntities())
                {
                    if (j.getEntity().equalsIgnoreCase("sys-currency"))
                        moneySpent = Float.valueOf(j.getValue());


                }
                if ( moneySpent != 0)
                {
                    EventQueue.sendEvent(new UpdateCashStatusEvent(AccountManager.WALLET,moneySpent));
                    somethingTriggered=true;
                }
            }


            // Request Summary
            if (i.getIntent().equalsIgnoreCase("HowMuchSpentFor"))
            {
                for (Entity j : response.getEntities())
                {
                    if (j.getEntity().equalsIgnoreCase("clothes"))
                        categorySpent = "clothes";

                    if (j.getEntity().equalsIgnoreCase("food"))
                        categorySpent = "food";


                }
                // if no category available, use geo location to suggest one
                if (!categorySpent.equals("") )
                {
                    somethingTriggered=true;
                    EventQueue.sendEvent(new     RequestCashStatusEvent(AccountManager.WALLET,categorySpent,false));
                }
            }

            // Request ##yes_show_atm
            if (i.getIntent().equalsIgnoreCase("yes_show_atm"))
            {
                    EventQueue.sendEvent(new GuideMeToDiBaATM());
            }

            if (i.getIntent().equalsIgnoreCase("YesAnyAtm") || i.getIntent().equalsIgnoreCase("YesStillAnyAtm"))
            {
                EventQueue.sendEvent(new GuideMeToNearestATM());
            }

            // GetSummary
            if (i.getIntent().equalsIgnoreCase("GetSummary"))
            {
                somethingTriggered=true;
                EventQueue.sendEvent(new RequestCashStatusEvent(AccountManager.WALLET,"",false));
            }

            // RunOutOfMoney
            if (i.getIntent().equalsIgnoreCase("RunOutOfMoney"))
            {
                somethingTriggered=true;
                EventQueue.sendEvent(new RequestCashStatusEvent(AccountManager.WALLET,"",true));
            }
        }
        return somethingTriggered;
    }

}
