package com.example.cwebe.dibachallenge.eventHandling.event;

import android.location.Location;

/**
 * Created by psherif on 20/11/16.
 */

public class UserChatLocationEvent extends  UserChatOutputEvent {

    public UserChatLocationEvent(String text) {
        super(text);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public UserChatLocationEvent(String text, Location location) {
        super(text);
        this.location = location;
    }

    Location location;

}
