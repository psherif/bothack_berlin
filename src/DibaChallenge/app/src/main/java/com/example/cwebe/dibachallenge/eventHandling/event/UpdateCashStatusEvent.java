package com.example.cwebe.dibachallenge.eventHandling.event;

import android.location.Location;

/**
 * Created by psherif on 19/11/16.
 */

public class UpdateCashStatusEvent implements IBaseEvent {

    public UpdateCashStatusEvent(String account, double accountBalance) {
        this.accountBalance = accountBalance;
        this.account = account;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public String getAccount() {
        return account;
    }

    double accountBalance;
    String account;
}
