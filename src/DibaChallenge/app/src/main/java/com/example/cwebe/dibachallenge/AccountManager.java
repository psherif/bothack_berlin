package com.example.cwebe.dibachallenge;

import android.app.usage.UsageEvents;
import android.location.Address;
import android.location.Location;
import android.provider.ContactsContract;

import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.IEventHandler;
import com.example.cwebe.dibachallenge.eventHandling.event.ATMInProximityEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.CashIsLowEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToDiBaATM;
import com.example.cwebe.dibachallenge.eventHandling.event.GuideMeToNearestATM;
import com.example.cwebe.dibachallenge.eventHandling.event.IBaseEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.ModifyCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.RequestCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UpdateCashStatusEvent;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextOutputEvent;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


class ATMTimerTask extends TimerTask {
    AccountManager acman;

    public ATMTimerTask(AccountManager acman){
        this.acman = acman;
    }

    public void run() {
        acman.TriggerAtm();
    }
}
/**
 * Created by christoph on 19/11/16.
 */

public class AccountManager implements IEventHandler{

    public static final String WALLET = "Wallet";
    public static final String DIBAACCOUNT = "DiBa";
    private List<Address> atmList = null;
    private boolean triggeredOnce = false;

    public AccountManager(AccountLogger logger){
        this.logger = logger;
        addAccount(WALLET);
        addAccount(DIBAACCOUNT);

        atmList = DataHandler.parseATMJson();

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(2016, 01, 01, 12, 00, 00);
        this.logger.logEvent(10000.0,DIBAACCOUNT,"Initial Balance",new Location(""),cal.getTime());
        this.logger.logEvent(200,WALLET,"CASH-WITHDRAWN",new Location(""),cal.getTime());
        cal.set(2016, 01, 03, 12, 00, 00);
        this.logger.logEvent(-60.0,WALLET,"Pants",new Location(""),cal.getTime());
        cal.set(2016, 01, 24, 12, 00, 00);
        this.logger.logEvent(-70.0,WALLET,"Sushi",new Location(""),cal.getTime());
        cal.set(2016, 03, 06, 12, 00, 00);
        this.logger.logEvent(160,WALLET,"CASH-WITHDRAWN",new Location(""),cal.getTime());
        this.logger.logEvent(-20.0,WALLET,"Trousers",new Location(""),cal.getTime());
        cal.set(2016, 03, 18, 12, 00, 00);
        this.logger.logEvent(-16.0,WALLET,"Sushi",new Location(""),cal.getTime());
        cal.set(2016, 04, 01, 12, 00, 00);
        this.logger.logEvent(30,WALLET,"CASH-WITHDRAWN",new Location(""),cal.getTime());
        this.logger.logEvent(-49.0,WALLET,"Pants",new Location(""),cal.getTime());
        cal.set(2016, 04, 11, 12, 00, 00);
        this.logger.logEvent(-19.0,WALLET,"Shirt",new Location(""),cal.getTime());
        cal.set(2016, 8, 01, 12, 00, 00);
        this.logger.logEvent(70,WALLET,"CASH-WITHDRAWN",new Location(""),cal.getTime());
        this.logger.logEvent(-4.0,WALLET,"Sushi",new Location(""),cal.getTime());
        cal.set(2016, 9, 01, 12, 00, 00);
        this.logger.logEvent(-16.0,WALLET,"Pants",new Location(""),cal.getTime());
        cal.set(2016, 10, 01, 12, 00, 00);
        this.logger.logEvent(80,WALLET,"CASH-WITHDRAWN",new Location(""),cal.getTime());
        this.logger.logEvent(-50.0,WALLET,"Trousers",new Location(""),cal.getTime());
        cal.set(2016, 11, 20, 12, 00, 00);
        this.logger.logEvent(-10.0,WALLET,"Sushi",new Location(""),cal.getTime());

        Timer timer = new Timer();

        TimerTask updateBall = new ATMTimerTask(this);
        timer.scheduleAtFixedRate(updateBall, 0, 5000);
    }

    public void TriggerAtm()
    {
        // event = new ATMInProximityEvent(atmList.get(30));
        //EventQueue.sendEvent(event);
        if(this.logger.getBalance(WALLET) < 20 && !triggeredOnce)
        {
            this.printMessage("There is an ATM near you, and you seem to need cash. Here is the address:");
            Random r = new Random();
            Address a = atmList.get(r.nextInt(atmList.size()-1));
            this.printMessage(a.getAddressLine(0)+"\n"+a.getLocality());
            triggeredOnce = true;
        }
    }

    public double transferMoney(String fromAccount, String toAccount, double amount, String purpose, Location location)
    {
        this.transaction(amount,toAccount,purpose,location);
        return this.transaction(-amount,fromAccount,purpose,location);
    }

    public void balaceAccountTo(String accountName, double accountBalance)
    {
        double balance = getAccountBalance(accountName);
        if(accountBalance <= 0)
        {
            this.transaction(-balance, accountName, "account balancing", new Location(""));
        }
        else
            this.transaction(accountBalance-balance, accountName, "account balancing", new Location(""));
    }

    public double getAccountBalance(String accountName)
    {
        if(!registeredAccounts.containsKey(accountName)) {
            addAccount(accountName);
            return 0.0;
        }
        return registeredAccounts.get(accountName).calculateBalance();
    }

    public double transaction(double amount, String accountname, String purpose, Location location)
    {
        if(!registeredAccounts.containsKey(accountname)) {
            addAccount(accountname);
        }
        double balance = registeredAccounts.get(accountname).transaction(amount,purpose,location);
        long secondsTillEmpty = this.logger.GetSecondsTillEmpty(accountname);
        if(balance <= 10.0)
        {
            CashIsLowEvent event = new CashIsLowEvent(accountname,balance);
            EventQueue.sendEvent(event);
        }
        else
            triggeredOnce = false;
        if(secondsTillEmpty < 100000)
        {
            CashIsLowEvent event = new CashIsLowEvent(accountname,balance,secondsTillEmpty);
            EventQueue.sendEvent(event);
        }
        if(balance < 0)
            this.balaceAccountTo(WALLET,0);

        return balance;
    }

    public void addAccount(String accountName)
    {
        Account account = new Account(accountName,this.logger);
        registeredAccounts.put( accountName,account);
    }

    public void printMessage(String message){
        UserTextOutputEvent event = new UserTextOutputEvent(message);
        EventQueue.sendEvent(event);
    }

    private AccountLogger logger;
    private HashMap<String,Account> registeredAccounts = new HashMap<String,Account>();

    @Override
    public void processEvent(IBaseEvent event) {
        if(event instanceof ModifyCashStatusEvent){
            ModifyCashStatusEvent modcash = (ModifyCashStatusEvent)event;
            this.transaction(modcash.getTransactionAmount(),modcash.getAccount(), modcash.getPurpose(), modcash.getLocation());
        }
        else if(event instanceof UpdateCashStatusEvent){
            UpdateCashStatusEvent upcash = (UpdateCashStatusEvent)event;
            this.balaceAccountTo(upcash.getAccount(), upcash.getAccountBalance());
        }
        else if(event instanceof GuideMeToNearestATM){
            //UpdateCashStatusEvent upcash = (UpdateCashStatusEvent)event;
            this.printMessage("There is an ATM near you:");
            Random r = new Random();
            Address a = atmList.get(r.nextInt(atmList.size()-1));
            this.printMessage(a.getAddressLine(0)+"\n"+a.getLocality());
        }
        else if(event instanceof GuideMeToDiBaATM){
            //GuideMeToDiBaATM upcash = (GuideMeToDiBaATM)event;
            this.printMessage("There is a DiBa ATM near you:");
            Random r = new Random();
            Address a = atmList.get(r.nextInt(atmList.size()-1));
            this.printMessage(a.getAddressLine(0)+"\n"+a.getLocality());
        }
        else if(event instanceof RequestCashStatusEvent){
            RequestCashStatusEvent upcash = (RequestCashStatusEvent)event;
            if(upcash.getCountdown())
            {
                long secs = this.logger.GetSecondsTillEmpty(upcash.getAccount());
                long days = Math.round(Math.ceil(secs / 60.0 / 60.0 / 24.0));
                String message = "only " +days+" days left";
                if(!upcash.getAccount().isEmpty())
                    message+=" in account "+upcash.getAccount();
                this.printMessage(message);
            }
            else
            {
                if(upcash.getPurpose().isEmpty())
                {
                    double balance = this.logger.getBalance(upcash.getAccount());
                    String message = "Balance is "+balance;
                    if(!upcash.getAccount().isEmpty())
                        message+=" in account "+upcash.getAccount();
                    this.printMessage(message);
                }
                else
                {
                    double spent = this.logger.moneySpentForItem(upcash.getPurpose());
                    String message = "You have spent "+ spent+" money";
                    if(!upcash.getPurpose().isEmpty())
                        message+= " on "+upcash.getPurpose();
                    this.printMessage(message);
                }
            }
        }
    }
}
