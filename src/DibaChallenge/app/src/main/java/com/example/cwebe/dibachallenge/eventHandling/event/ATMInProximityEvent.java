package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

import android.location.Address;

/**
 *  Called Background services:
 *  geo location triggered
 */
public class ATMInProximityEvent implements IBaseEvent {

    public ATMInProximityEvent(Address address){
        this.address = address;
    }

    Address address;
}
