package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

public class UserOutputEvent implements IBaseEvent {
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserOutputEvent(String text) {
        this.text = text;
    }

    String text;
}
