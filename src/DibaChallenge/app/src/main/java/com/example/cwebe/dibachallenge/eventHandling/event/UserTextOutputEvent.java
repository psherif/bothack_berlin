package com.example.cwebe.dibachallenge.eventHandling.event;

/**
 * Created by psherif on 19/11/16.
 */

/**
 *  Trigger UI text output
 */
public class UserTextOutputEvent extends UserOutputEvent {
    public UserTextOutputEvent(String text) {
        super(text);
    }
}
