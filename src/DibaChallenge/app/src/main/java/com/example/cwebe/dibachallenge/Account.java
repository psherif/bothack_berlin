package com.example.cwebe.dibachallenge;

import android.location.Location;
import android.util.Log;

import com.example.cwebe.dibachallenge.eventHandling.EventQueue;
import com.example.cwebe.dibachallenge.eventHandling.event.UserTextOutputEvent;

/**
 * Created by christoph on 19/11/16.
 */

public class Account {

    public Account(String accountName, AccountLogger logger){
        this.balance = 0.0;
        this.accountName = accountName;
        this.logger = logger;
        this.calculateBalance();
        UserTextOutputEvent event = new UserTextOutputEvent("Balance is: " + this.getBalance());
        EventQueue.sendEvent(event);
        this.calculateBalance();
    }

    public double transaction(double amount, String purpose, Location location){
        this.balance += amount;
        if(this.logger != null)
            this.logger.logEvent(amount, this.accountName, purpose, location);
        return this.getBalance();
    }

    public double getBalance() {
        calculateBalance();
        return this.balance;
    }

    public double calculateBalance(){
        this.balance = this.logger.getBalance(this.accountName);
        return this.balance;
    }

    public String getAccountName() {
        return accountName;
    }

    private String accountName;
    private double balance;
    private AccountLogger logger;

}
