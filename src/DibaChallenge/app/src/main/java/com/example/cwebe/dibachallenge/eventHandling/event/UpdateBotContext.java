package com.example.cwebe.dibachallenge.eventHandling.event;

import java.util.Map;

/**
 * Created by psherif on 19/11/16.
 */

public class UpdateBotContext implements IBaseEvent {
    Map<String, Object> context;
    public UpdateBotContext(Map<String, Object> context)
    {
        this.context = context;
    }

    public Map<String,Object> GetContext()
    {
        return this.context;
    }
}
